"""
Main script for franceculturebot.
"""

#Imports pour demandez_le_programme()
import os
import sys
import locale
import configparser
from datetime import datetime, date, time
import tweepy

#Imports pour chercher_url_emission()
from urllib import request,parse
from urllib.request import urlopen
from bs4 import BeautifulSoup


def chercher_url_emission(nom_emission):
    """
    Pour une émission de France Culture, fourni l'adresse URL de la diffusion du jour.
    Entrée :
        -nom_emission       str

    Sortie :
        -url_emission       str
    """
    url = 'https://www.franceculture.fr/programmes'
    liste_programmes = []

    html_programmes = urlopen(url)
    soup = BeautifulSoup(html_programmes, "lxml")

    for link in soup.find_all('a', "program-item-content-elements-infos"):
        liste_programmes = liste_programmes + [link.get('href')]

    for i in range(len(liste_programmes)):
        if liste_programmes[i][11:11+len(nom_emission)] == nom_emission:
            url_emission = "https://www.franceculture.fr" + liste_programmes[i]

    return url_emission


def demandez_le_programme():

    """
    Fourni heure par heure le programme de France Culture.
    Sortie :
        -le_programme     str
    """

    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

    ajd = datetime.today()
    y = ajd.year
    mo = ajd.month
    d = ajd.day
    h = ajd.hour
    mi = ajd.minute
    n_semaine = ajd.isocalendar()[1]

    nom_emission = ""
    url_emission = ""

    """
    Programmes des lundi, mardi, mercredi et jeudi
    """

    if ajd.isoweekday() == 1 or ajd.isoweekday() == 2 or ajd.isoweekday() == 3 or ajd.isoweekday() == 4:

        if h == 5 and mi == 0:
            nom_emission = "les-cours-du-college-de-france"
            le_programme = "Bonjour ! Il est 5h, l'heure des Cours du Collège de France sur France Culture.\n  " + chercher_url_emission(nom_emission)

        if h == 6 and mi == 0:
            nom_emission = "les-petits-matins"
            le_programme = "Bonjour ! Il est 6h, l'heure des Petits matins sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 7 and mi == 0:
            nom_emission = "les-matins"
            le_programme = "Il est 7h, l'heure des Matins sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 0:
            nom_emission = "journal-de-8h"
            le_programme = "Il est 8h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 0:
            nom_emission = "journal-de-9h"
            le_programme = "Il est 9h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 5:
            nom_emission = "le-cours-de-lhistoire"
            le_programme = "Il est 9h05, l'heure du Cours de l'histoire sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 10 and mi == 0:
            nom_emission = "les-chemins-de-la-philosophie"
            le_programme = "Il est 10h, l'heure des Chemins de la philosophie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 11 and mi == 0:
            nom_emission = "cultures-monde"
            le_programme = "Il est 11h, l'heure de Cultures Monde sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 0:
            nom_emission = "la-grande-table-culture"
            le_programme = "Il est midi, l'heure de La Grande table culture sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 30:
            nom_emission = "journal-de-12h30"
            le_programme = "Il est 12h30, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 55:
            nom_emission = "la-grande-table-idees"
            le_programme = "Il est 12h55, l'heure de La Grande table idées sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 13 and mi == 30:
            nom_emission = "les-pieds-sur-terre"
            le_programme = "Il est 13h30, l'heure des Pieds sur terre sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 14 and mi == 0:
            nom_emission = "entendez-vous-leco"
            le_programme = "Il est 14h, l'heure d'Entendez-vous l'éco sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 15 and mi == 0:
            nom_emission = "la-compagnie-des-oeuvres"
            le_programme = "Il est 15h, l'heure de La Compagnie des oeuvres sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 0:
            nom_emission = "la-methode-scientifique"
            le_programme = "Il est 16h00, l'heure de La Méthode scientifique sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 2:
            nom_emission = "le-journal-des-sciences"
            le_programme = "Il est 16h00, l'heure du journal des sciences sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 17 and mi == 0:
            nom_emission = "lsd-la-serie-documentaire"
            le_programme = "Il est 17h, l'heure de LSD, La série documentaire sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 0:
            nom_emission = "journal-de-18h"
            le_programme = "Il est 18h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 20:
            nom_emission = "le-temps-du-debat"
            le_programme = "Il est 18h20, l'heure du Temps du débat sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 19 and mi == 0:
            nom_emission = "la-dispute"
            le_programme = "Il est 19h, l'heure de La Dispute sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 0:
            nom_emission = "a-voix-nue"
            le_programme = "Il est 20h, l'heure d'A voix nue sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 30:
            nom_emission = "fictions-le-feuilleton"
            le_programme = "Il est 20h30, l'heure du Feuilleton sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 55:
            nom_emission = "les-carnets-de-la-creation"
            le_programme = "Il est 20h55, l'heure des Carnets de la création sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 21 and mi == 0:
            nom_emission = "par-les-temps-qui-courent"
            le_programme = "Il est 21h, l'heure de Par les temps qui courent sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 22 and mi == 0:
            nom_emission = "journal-de-22h"
            le_programme = "Il est 22h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 22 and mi == 15:
            nom_emission = "matieres-a-penser"
            le_programme = "Il est 22h15, l'heure des Matières à penser sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 0:
            nom_emission = "lsd-la-serie-documentaire"
            le_programme = "Il est 23h, l'heure de la rediffusion de LSD, La série documentaire sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 55:
            nom_emission = "la-conclusion"
            le_programme = "Il est 23h55, l'heure de la rediffusion de La Conclusion sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 59: # on lance le programme de nuit, on peut tricher un peu ;)
            le_programme = "Il est minuit, c'est La Nuit sur France Culture.\n  https://www.franceculture.fr/emissions/les-nuits-de-france-culture"

        else:
            sys.exit()


    """
    Programme du vendredi
    """

    if ajd.isoweekday() == 5:

        if h == 5 and mi == 0:
            nom_emission = "les-cours-du-college-de-france"
            le_programme = "Bonjour ! Il est 5h, l'heure des Cours du Collège de France sur France Culture.\n " + chercher_url_emission(nom_emission)

        if h == 6 and mi == 0:
            nom_emission = "les-petits-matins"
            le_programme = "Bonjour ! Il est 6h, l'heure des Petits matins sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 7 and mi == 0:
            nom_emission = "les-matins"
            le_programme = "Il est 7h, l'heure des Matins sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 0:
            nom_emission = "journal-de-8h"
            le_programme = "Il est 8h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 0:
            nom_emission = "journal-de-9h"
            le_programme = "Il est 9h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 5:
            nom_emission = "le-cours-de-lhistoire"
            le_programme = "Il est 9h05, l'heure du Cours de l'histoire sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 10 and mi == 0:
            nom_emission = "les-chemins-de-la-philosophie"
            le_programme = "Il est 10h, l'heure des Chemins de la philosophie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 11 and mi == 0:
            nom_emission = "cultures-monde"
            le_programme = "Il est 11h, l'heure de Cultures Monde sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 0:
            nom_emission = "la-grande-table-culture"
            le_programme = "Il est midi, l'heure de La Grande table culture sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 30:
            nom_emission = "journal-de-12h30"
            le_programme = "Il est 12h30, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 55:
            nom_emission = "la-grande-table-idees"
            le_programme = "Il est 12h55, l'heure de La Grande table idées sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 13 and mi == 30:
            nom_emission = "les-pieds-sur-terre"
            le_programme = "Il est 13h30, l'heure des Pieds sur terre sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 14 and mi == 0:
            nom_emission = "entendez-vous-leco"
            le_programme = "Il est 14h, l'heure d'Entendez-vous l'éco sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 15 and mi == 0:
            nom_emission = "la-compagnie-des-poetes"
            le_programme = "Il est 15h, l'heure de La Compagnie des poètes sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 0:
            nom_emission = "la-methode-scientifique"
            le_programme = "Il est 16h00, l'heure de La Méthode scientifique sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 2:
            nom_emission = "le-journal-des-sciences"
            le_programme = "Il est 16h00, l'heure du journal des sciences sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 17 and mi == 0:
            nom_emission = "grand-reportage"
            le_programme = "Il est 17h, l'heure du Grand Reportage sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 0:
            nom_emission = "journal-de-18h"
            le_programme = "Il est 18h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 20:
            nom_emission = "le-temps-du-debat"
            le_programme = "Il est 18h20, l'heure du Temps du débat sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 19 and mi == 0:
            nom_emission = "la-dispute"
            le_programme = "Il est 19h, l'heure de La Dispute sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 0:
            nom_emission = "a-voix-nue"
            le_programme = "Il est 20h, l'heure d'A voix nue sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 30:
            nom_emission = "fictions-le-feuilleton"
            le_programme = "Il est 20h30, l'heure du Feuilleton sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 55:
            nom_emission = "les-carnets-de-la-creation"
            le_programme = "Il est 20h55, l'heure des Carnets de la création sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 21 and mi == 0:
            nom_emission = "par-les-temps-qui-courent"
            le_programme = "Il est 21h, l'heure de Par les temps qui courent sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 22 and mi == 0:
            nom_emission = "journal-de-22h"
            le_programme = "Il est 22h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 22 and mi == 15:
            nom_emission = "matieres-a-penser"
            le_programme = "Il est 22h15, l'heure des Matières à penser sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 0:
            nom_emission = "latelier-fiction"
            le_programme = "Il est 23h, l'heure de L'Atelier fiction sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 59: # on lance le programme de nuit, on peut tricher un peu ;)
            le_programme = "Il est minuit, c'est La Nuit sur France Culture.\n  https://www.franceculture.fr/emissions/les-nuits-de-france-culture"

        else:
            sys.exit()


    """
    Programme du samedi
    """

    if ajd.isoweekday() == 6:

        if h == 7 and mi == 0:
            nom_emission = "les-matins-du-samedi"
            le_programme = "Il est 7h, l'heure des Matins du samedi sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 0:
            nom_emission = "journal-de-8h"
            le_programme = "Il est 8h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 0:
            nom_emission = "journal-de-9h"
            le_programme = "Il est 9h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 5:
            nom_emission = "repliques"
            le_programme = "Il est 9h05, l'heure de Répliques sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 10 and mi == 0:
            nom_emission = "concordance-des-temps"
            le_programme = "Il est 10h, l'heure de Concordance des temps sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 11 and mi == 0:
            nom_emission = "affaires-etrangeres"
            le_programme = "Il est 11h, l'heure d'Affaires étrangères sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 0:
            nom_emission = "politique"
            le_programme = "Il est midi, l'heure de Politique ! sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 30:
            nom_emission = "journal-de-12h30"
            le_programme = "Il est 12h30, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 45:
            nom_emission = "la-suite-dans-les-idees"
            le_programme = "Il est 12h55, l'heure de La Suite dans les idées sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 13 and mi == 30:
            nom_emission = "une-histoire-particuliere-un-recit-documentaire-en-deux-parties"
            le_programme = "Il est 13h30, l'heure d'Une histoire particulière sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 14 and mi == 0:
            nom_emission = "plan-large"
            le_programme = "Il est 14h, l'heure de Plan large sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 15 and mi == 0:
            nom_emission = "une-vie-une-oeuvre"
            le_programme = "Il est 15h, l'heure de Toute une vie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 0:
            nom_emission = "la-conversation-scientifique"
            le_programme = "Il est 16h00, l'heure de La Conversation scientifique sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 17 and mi == 0:
            nom_emission = "le-temps-des-ecrivains"
            le_programme = "Il est 17h, l'heure du Temps des écrivains France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 0:
            nom_emission = "journal-de-18h"
            le_programme = "Il est 18h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 10:
            nom_emission = "avis-critique"
            le_programme = "Il est 18h10, l'heure d'Avis critique sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 19 and mi == 0:
            nom_emission = "juke-box"
            le_programme = "Il est 19h, l'heure de Juke-Box sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 0:
            nom_emission = "les-masterclasses"
            le_programme = "Il est 20h, l'heure des Masterclasses sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 21 and mi == 0:
            nom_emission = "fictions-samedi-noir"
            le_programme = "Il est 21h, l'heure de Samedi noir sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 22 and mi == 0:
            nom_emission = "mauvais-genres"
            le_programme = "Il est 22h, l'heure de Mauvais genres sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 0:
            nom_emission = "une-vie-une-oeuvre"
            le_programme = "Il est 23h, l'heure de la rediffusion de Toute une vie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 59: # on lance le programme de nuit, on peut tricher un peu ;)
            le_programme = "Il est minuit, c'est La Nuit sur France Culture.\n  https://www.franceculture.fr/emissions/les-nuits-de-france-culture"

        else:
            sys.exit()


    """
    Programme du dimanche
    """
    
    if ajd.isoweekday() == 7:

        if h == 7 and mi == 0:
            nom_emission = "journal-de-7-h"
            le_programme = "Il est 7h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 7 and mi == 5:
            nom_emission = "questions-dislam"
            le_programme = "Il est 7h05, l'heure de Questions d'islam sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 0:
            nom_emission = "journal-de-8h"
            le_programme = "Il est 8h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 7:

            if n_semaine%2 == 0:
                nom_emission = "chretiens-dorient"
                le_programme = "Il est 8h07, l'heure de Chrétiens d'Orient sur France Culture.\n " + chercher_url_emission(nom_emission)

            else:
                nom_emission = "orthodoxie"
                le_programme = "Il est 8h07, l'heure d'Orthodoxie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 8 and mi == 30:
            nom_emission = "service-protestant"
            le_programme = "Il est 8h30, l'heure du Service protestant sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 0:
            nom_emission = "journal-de-9h"
            le_programme = "Il est 9h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 5:
            nom_emission = "talmudiques"
            le_programme = "Il est 9h05, l'heure de Talmudiques sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 9 and mi == 40:
            nom_emission = "divers-aspects-de-la-pensee-contemporaine"
            le_programme = "Il est 9h05, l'heure de Divers aspects de la pensée contemporaine sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 10 and mi == 0:
            nom_emission = "journal-de-10h"
            le_programme = "Il est 10h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 10 and mi == 5:
            nom_emission = "la-messe"
            le_programme = "Il est 10h05, l'heure de La Messe sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 11 and mi == 0:
            nom_emission = "lesprit-public"
            le_programme = "Il est 11h, l'heure de L'Esprit public sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 0:
            nom_emission = "les-bonnes-choses"
            le_programme = "Il est midi, l'heure des Bonnes choses sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 30:
            nom_emission = "journal-de-12h30"
            le_programme = "Il est 12h30, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 12 and mi == 45:
            nom_emission = "signes-des-temps"
            le_programme = "Il est 12h45, l'heure de Signes des temps sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 13 and mi == 30:
            nom_emission = "une-histoire-particuliere-un-recit-documentaire-en-deux-parties"
            le_programme = "Il est 13h30, l'heure d'Une histoire particulière sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 14 and mi == 0:
            nom_emission = "lart-est-la-matiere"
            le_programme = "Il est 14h, l'heure de L'Art et la matière sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 15 and mi == 0:
            nom_emission = "personnages-en-personne"
            le_programme = "Il est 15h, l'heure de Personnages en personne sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 15 and mi == 30:
            nom_emission = "le-rayon-bd"
            le_programme = "Il est 15h30, l'heure du Rayon BD sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 16 and mi == 0:
            nom_emission = "de-cause-a-effets-le-magazine-de-lenvironnement"
            le_programme = "Il est 16h00, l'heure De cause à effets, le magazine de l'environnement sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 17 and mi == 0:
            nom_emission = "etre-et-savoir"
            le_programme = "Il est 17h, l'heure d'Être et savoir sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 0:
            nom_emission = "journal-de-18h"
            le_programme = "Il est 18h, l'heure du journal sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 18 and mi == 10:
            nom_emission = "soft-power"
            le_programme = "Il est 18h10, l'heure de Soft Power sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 19 and mi == 30:
            nom_emission = "carbone-14-le-magazine-de-larcheologie"
            le_programme = "Il est 19h30, l'heure de Carbone 14, le magazine de l'archéologie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 20 and mi == 0:
            nom_emission = "tous-en-scene"
            le_programme = "Il est 20h, l'heure de Tous en scène sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 21 and mi == 0:
            nom_emission = "fictions-theatre-et-cie"
            le_programme = "Il est 21h, l'heure de Théâtre et Cie sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 0:
            nom_emission = "lexperience"
            le_programme = "Il est 23h, l'heure de L'Expérience sur France Culture.\n " + chercher_url_emission(nom_emission)

        elif h == 23 and mi == 59: # on lance le programme de nuit, on peut tricher un peu ;)
            le_programme = "Il est minuit, c'est La Nuit sur France Culture.\n  https://www.franceculture.fr/emissions/les-nuits-de-france-culture"

        else:
            sys.exit()

    return le_programme


if __name__ == "__main__":

    # read config file
    franceculturebot_project_path = os.path.dirname(os.path.realpath(__file__))
    config = configparser.ConfigParser()
    config.read(os.path.join(franceculturebot_project_path, "config", "config.ini"))
    consumer_key = config.get("access", "consumer_key")
    consumer_secret = config.get("access", "consumer_secret")
    access_token = config.get("access", "access_token")
    access_token_secret = config.get("access", "access_token_secret")

    # twitter authentification
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    le_programme = demandez_le_programme()

    api.update_status(status=le_programme)
