# franceculturebot

Un bot qui tweete le programme de France Culture.

## Requirements:

*   python3
*   tweepy
*   beautifulsoup4
*   lxml
*   a twitter developer account